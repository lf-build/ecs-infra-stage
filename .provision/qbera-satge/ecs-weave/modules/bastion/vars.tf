variable "project_name" {
  description = "The project name."
}

variable "project_env" {
  description = "The project env."
}

variable "key_name" {
  description = "Name of AWS key pair"
}

variable "instance_type" {
  default     = "t2.small"
  description = "AWS instance type"
}

variable "vpc_id" {
  description = "The VPC ID"
}

variable "vpc_public_subnet_id" {
  description = "The VPC subnet ids"
}

variable "amazon_ami_name" {
  description = "Amazon AMI Name"
  default     = "amzn-ami-hvm-2016.09.*"
}

variable "enable_bastion_disk1" {}
variable "bastion_disk1_size" {}
