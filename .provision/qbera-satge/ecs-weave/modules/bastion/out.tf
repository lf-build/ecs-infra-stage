
output "bastion_ip" {
  value = "${aws_eip.bastion.public_ip}"
}

output "bastion_key" {
  value = "${var.key_name}"
}
