

whitelisted_cidr_list = ["182.73.101.138/32"]

# Ubuntu 14.04
# To find ami id
# search for ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server-20170405
# under community AMI in that aws region.
base_amis = {
  us-east-1 = "ami-772aa961"
  ap-southeast-1 = "ami-0a19a669"
}
az_count = 2

ecs_ansible_s3_region = "ap-southeast-1" # this scripts are for installation of docker,fluentd,sysdig etc which is common across all the regions, hence no need to specify the region
ecs_ansible_s3_bucket = "ce-src-ansible" # this bucket should not be deleted
ecs_ansible_s3_prefix = "/ecs-ansible" # it should be empty or should start with /

instance_type = "m4.2xlarge"
asg_min = 1
asg_desired = 1
asg_max = 1

root_disk_size = 500
alrm_ecs_memr_thresold = 90
asg_memr_high_thresold = 85

user_data_type = "custom1"

http_enabled = false
https_enabled = true
lbsg_http_enabled = false
lbsg_https_enabled = true
ssl_cert_arn = "arn:aws:acm:ap-southeast-1:893739631033:certificate/4f515899-07ec-4535-b902-eba0e025f6f9"
#ssl_cert_arn = "arn:aws:acm:ap-southeast-1:993978665313:certificate/352865b8-cedd-4494-919f-0ee773ab7945" 
