### Security

resource "aws_security_group" "ops_lb" {
  description = "Controls access to the Operations ELB"

  vpc_id = "${var.vpc_id}"
  name   = "asg-${var.project_name}-${var.project_env}-ops-lb"

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_security_group_rule" "ops_lb_whitelist" {
  type            = "ingress"
  from_port       = 0
  to_port         = 0
  protocol        = "-1"
  cidr_blocks     = ["${var.whitelisted_cidr_list}"]
  security_group_id = "${aws_security_group.ops_lb.id}"
}

resource "aws_security_group_rule" "ops_lb_6060" {
  count           = "${var.lbsg_http_enabled}"
  type            = "ingress"
  from_port       = 6060
  to_port         = 6060
  protocol        = "tcp"
  cidr_blocks     = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.ops_lb.id}"
}
resource "aws_security_group_rule" "ops_lb_6443" {
  count           = "${var.lbsg_https_enabled}"
  type            = "ingress"
  from_port       = 6443
  to_port         = 6443
  protocol        = "tcp"
  cidr_blocks     = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.ops_lb.id}"
}
resource "aws_security_group_rule" "ops_lb_7070" {
  count           = "${var.lbsg_http_enabled}"
  type            = "ingress"
  from_port       = 7070
  to_port         = 7070
  protocol        = "tcp"
  cidr_blocks     = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.ops_lb.id}"
}
resource "aws_security_group_rule" "ops_lb_7443" {
  count           = "${var.lbsg_https_enabled}"
  type            = "ingress"
  from_port       = 7443
  to_port         = 7443
  protocol        = "tcp"
  cidr_blocks     = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.ops_lb.id}"
}
resource "aws_security_group_rule" "ops_lb_8080" {
  count           = "${var.lbsg_http_enabled}"
  type            = "ingress"
  from_port       = 8080
  to_port         = 8080
  protocol        = "tcp"
  cidr_blocks     = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.ops_lb.id}"
}
resource "aws_security_group_rule" "ops_lb_8443" {
  count           = "${var.lbsg_https_enabled}"
  type            = "ingress"
  from_port       = 8443
  to_port         = 8443
  protocol        = "tcp"
  cidr_blocks     = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.ops_lb.id}"
}
resource "aws_security_group_rule" "ops_lb_9090" {
  count           = "${var.lbsg_http_enabled}"
  type            = "ingress"
  from_port       = 9090
  to_port         = 9090
  protocol        = "tcp"
  cidr_blocks     = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.ops_lb.id}"
}
resource "aws_security_group_rule" "ops_lb_9443" {
  count           = "${var.lbsg_https_enabled}"
  type            = "ingress"
  from_port       = 9443
  to_port         = 9443
  protocol        = "tcp"
  cidr_blocks     = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.ops_lb.id}"
}

resource "aws_security_group" "ui_lb" {
  description = "Controls access to the UI ELB"

  vpc_id = "${var.vpc_id}"
  name   = "asg-${var.project_name}-${var.project_env}-ui-lb"

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_security_group" "app_lb" {
  description = "Controls access to the App Workflow ELB"

  vpc_id = "${var.vpc_id}"
  name   = "asg-${var.project_name}-${var.project_env}-app-lb"

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_security_group_rule" "app_lb_80" {
  count           = "${var.lbsg_http_enabled}"
  type            = "ingress"
  from_port       = 80
  to_port         = 80
  protocol        = "tcp"
  cidr_blocks     = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.app_lb.id}"
}

resource "aws_security_group_rule" "app_lb_443" {
  count           = "${var.lbsg_https_enabled}"
  type            = "ingress"
  from_port       = 443
  to_port         = 443
  protocol        = "tcp"
  cidr_blocks     = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.app_lb.id}"
}

resource "aws_security_group" "orbit_lb" {
  description = "Controls access to the Orbit identity ELB"

  vpc_id = "${var.vpc_id}"
  name   = "asg-${var.project_name}-${var.project_env}-orbit-lb"

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_security_group_rule" "orbit_lb_80" {
  count           = "${var.lbsg_http_enabled}"
  type            = "ingress"
  from_port       = 80
  to_port         = 80
  protocol        = "tcp"
  cidr_blocks     = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.orbit_lb.id}"
}

resource "aws_security_group_rule" "orbit_lb_443" {
  count           = "${var.lbsg_https_enabled}"
  type            = "ingress"
  from_port       = 443
  to_port         = 443
  protocol        = "tcp"
  cidr_blocks     = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.orbit_lb.id}"
}

resource "aws_security_group" "verify_lb" {
  description = "Controls access to the Verification Portal ELB"

  vpc_id = "${var.vpc_id}"
  name   = "asg-${var.project_name}-${var.project_env}-verify-lb"

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_security_group_rule" "verify_lb_80" {
  count           = "${var.lbsg_http_enabled}"
  type            = "ingress"
  from_port       = 80
  to_port         = 80
  protocol        = "tcp"
  cidr_blocks     = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.verify_lb.id}"
}

resource "aws_security_group_rule" "verify_lb_443" {
  count           = "${var.lbsg_https_enabled}"
  type            = "ingress"
  from_port       = 443
  to_port         = 443
  protocol        = "tcp"
  cidr_blocks     = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.verify_lb.id}"
}

resource "aws_security_group" "nodes" {
  description = "Controls access to ecs instances"
  vpc_id      = "${var.vpc_id}"
  name        = "asg-${var.project_name}-${var.project_env}-nodes"

  ingress {
    protocol  = "-1"
    from_port = 0
    to_port   = 0
    self = true
  }

  ingress {
    protocol  = "-1"
    from_port = 0
    to_port   = 0
    security_groups = [
      "${aws_security_group.ops_lb.id}",
      "${aws_security_group.ui_lb.id}",
      "${aws_security_group.app_lb.id}",
      "${aws_security_group.orbit_lb.id}",
      "${aws_security_group.verify_lb.id}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}
