
instance_type = "m4.2xlarge"
asg_min = 1
asg_desired = 1
asg_max = 1

root_disk_size = 500
alrm_ecs_memr_thresold = 90
asg_memr_high_thresold = 85

user_data_type = "custom1"

http_enabled = false
https_enabled = true
lbsg_http_enabled = false
lbsg_https_enabled = true
ssl_cert_arn = "arn:aws:acm:ap-southeast-1:893739631033:certificate/4f515899-07ec-4535-b902-eba0e025f6f9"
#ssl_cert_arn = "arn:aws:acm:ap-southeast-1:993978665313:certificate/352865b8-cedd-4494-919f-0ee773ab7945" 
