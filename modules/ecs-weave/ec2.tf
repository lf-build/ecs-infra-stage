## EC2

resource "aws_autoscaling_group" "nodes" {
  name                 = "asg-${var.project_name}-${var.project_env}-nodes"
  vpc_zone_identifier  = ["${var.vpc_private_subnet_ids}"]
  min_size             = "${var.asg_min}"
  max_size             = "${var.asg_max}"
  desired_capacity     = "${var.asg_desired}"
  launch_configuration = "${aws_launch_configuration.nodes.name}"
  target_group_arns    = ["${aws_alb_target_group.weavescope.id}"]
  depends_on           = ["aws_cloudwatch_log_group.main"]

  tag {
    key                 = "Name"
    value               = "ec2-${var.project_name}-${var.project_env}-nodes"
    propagate_at_launch = true
  }
  tag {
    key                 = "Project"
    value               = "${var.project_name}"
    propagate_at_launch = true
  }
  tag {
    key                 = "ENV"
    value               = "${var.project_env}"
    propagate_at_launch = true
  }
  tag {
    key                 = "ManagedBy"
    value               = "terraform"
    propagate_at_launch = true
  }
  tag {
    key                 = "Autoscaling"
    value               = "yes"
    propagate_at_launch = true
  }
}

data "template_file" "nodes_user_data" {
  template = "${file("${path.module}/files/user-data.${var.user_data_type}")}"

  vars {
    ecs_cluster_name   = "${aws_ecs_cluster.main.name}"
    aws_region         = "${var.aws_region}"
    ecs_init_version   = ""
    ecs_ansible_path   = "s3://${var.ecs_ansible_s3_bucket}${var.ecs_ansible_s3_prefix}"
    ecs_ansible_s3_region = "${var.ecs_ansible_s3_region}"
  }
}

resource "aws_launch_configuration" "nodes" {
  name                        = "lc-${var.project_name}-${var.project_env}-nodes"
  key_name                    = "${var.key_name}"
  image_id                    = "${lookup(var.base_amis, var.aws_region)}"
  instance_type               = "${var.instance_type}"
  iam_instance_profile        = "${aws_iam_instance_profile.nodes.name}"
  user_data                   = "${data.template_file.nodes_user_data.rendered}"
  security_groups             = ["${aws_security_group.nodes.id}"]

  lifecycle {
    create_before_destroy = true
  }

  root_block_device {
    volume_type = "gp2"
    volume_size = "${var.root_disk_size}"
  }
}
